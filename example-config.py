class Config:
    USER_AGENT = 'A Quite Interesting Bot by /u/Mr_Dionysus'
    ADMIN = '/u/Mr_Dionysus'

    ACTIVE_SUB = 'quiteinteresting'

    RSS = 'http://feeds.soundcloud.com/users/soundcloud:users:11400353/sounds.rss?#'
    DATABASE = 'data.db'

    USERNAME = 'AQuiteInterestingBot'
    PASSWORD = '***'

    OAUTH_ID = '***'
    OAUTH_SECRET = '***'
    OAUTH_STATE = '***'
    REDIRECT_URI = 'http://127.0.0.1:65010/authorize_callback'

    SCOPES = 'edit submit'

    REFRESH_TOKEN = '*****'
