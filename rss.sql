DROP TABLE IF EXISTS entries;
CREATE TABLE entries(
  thing_id TEXT,
  title TEXT,
  link TEXT,
  published TIMESTAMP
);