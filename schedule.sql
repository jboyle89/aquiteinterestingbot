DROP TABLE IF EXISTS episodes;
CREATE TABLE episodes(
  episode_number INT PRIMARY KEY,
  season TEXT,
  title TEXT,
  guests TEXT,
  post TIMESTAMP,
  is_posted INT
);