import datetime
import sqlite3

import feedparser
import praw

from config import Config


class QIBot:
    def __init__(self):
        self.reddit = praw.Reddit(user_agent=Config.USER_AGENT)
        self.reddit.set_oauth_app_info(Config.OAUTH_ID, Config.OAUTH_SECRET, Config.REDIRECT_URI)

    def get_previous_rss(self, conn):
        cur = conn.cursor()
        feed = feedparser.parse(Config.RSS)
        for entry in feed.entries:
            cur.execute('INSERT INTO entries(thing_id, title, link, published) VALUES(?,?,?,?)',
                        (None, entry.title, entry.link, entry.published))

        conn.commit()

    def init_db(self):
        conn = sqlite3.connect(Config.DATABASE, detect_types=sqlite3.PARSE_DECLTYPES)
        conn.executescript(open('rss.sql').read())
        self.get_previous_rss(conn=conn)

        conn.executescript(open('schedule.sql').read())
        conn.commit()

    def manage_rss(self):
        conn = sqlite3.connect(Config.DATABASE, detect_types=sqlite3.PARSE_DECLTYPES)
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute('SELECT title FROM entries')

        entries = cur.fetchall()
        l = [s["title"] for s in entries]

        feed = feedparser.parse(Config.RSS)
        for entry in feed.entries:
            if entry.title not in l:
                try:
                    self.reddit.refresh_access_information(Config.REFRESH_TOKEN)
                    s = self.reddit.submit(subreddit=Config.ACTIVE_SUB, title='[No Such Thing as a Fish] '
                                                                              + entry.title, url=entry.link)
                    s.add_comment('This post was made automatically. If there are any questions/comments, please message '
                                  + Config.ADMIN + '.')
                    print("Post Created in {} :: {}".format(Config.ACTIVE_SUB, s.url))
                    cur.execute('INSERT INTO entries(thing_id, title, link, published) VALUES (?,?,?,?)',
                                (s.id, entry.title, entry.link, entry.published))
                except praw.errors.AlreadySubmitted:
                    cur.execute('INSERT INTO entries(thing_id, title, link, published) VALUES (?,?,?,?)',
                                (s.id, entry.title, entry.link, entry.published))
                    print("Post Already Created in {} :: {}".format(Config.ACTIVE_SUB, s.url))

        conn.commit()

    def check_schedule(self):
        conn = sqlite3.connect(Config.DATABASE, detect_types=sqlite3.PARSE_DECLTYPES)
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()

        cur.execute('SELECT * FROM episodes WHERE is_posted=0')
        for row in cur:
            if row['post'] is not None and row['post'] <= datetime.datetime.now():
                cur.execute('UPDATE episodes SET is_posted=1 WHERE episode_number=?', (row['episode_number'],))
                self.create_discussion_post(row)
                break

        conn.commit()

    def create_discussion_post(self, episode):
        title = "[Episode Discussion] Series {} : {}".format(episode['season'], episode['title'])
        with open('selftext.txt') as f:
            text = f.read().format(title=episode['title'], airdate=datetime.datetime.now().strftime('%B %d, %Y'),
                                   guests=episode['guests'])
        self.reddit.refresh_access_information(Config.REFRESH_TOKEN)
        s = self.reddit.submit(Config.ACTIVE_SUB, title, text=text)
        s.add_comment('This post was made automatically. If there are any questions/comments, please message ' +
                      Config.ADMIN + '.')
        s.set_flair(flair_text='NSTAAF')
        print("Post Created in {} :: {}".format(Config.ACTIVE_SUB, s.url))

    def main(self):
        running = True

        while running:
            try:
                self.manage_rss()
                self.check_schedule()
            except KeyboardInterrupt:
                running = False
            except Exception as e:
                now = datetime.datetime.now()
                print(now.strftime("%m-%d-%Y %H:%M") + ' Error: ' + str(e))
                continue


if __name__ == '__main__':
    qi = QIBot()
    qi.main()
